package xmljson.convert;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import xmljson.protocol.EmptyElement;

public class EmptyElementSerializer extends JsonSerializer<EmptyElement> {
  @Override
  public void serialize(EmptyElement value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {
    jgen.writeBoolean(true);
  }
}
