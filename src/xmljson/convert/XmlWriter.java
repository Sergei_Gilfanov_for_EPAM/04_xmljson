package xmljson.convert;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;
import java.io.OutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import xmljson.protocol.Catalog;

public class XmlWriter {
  static SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
  JAXBContext context;
  Marshaller marshaller;
  Schema schema;

  public XmlWriter() throws JAXBException, SAXException {
    context = JAXBContext.newInstance(Catalog.class);
    marshaller = context.createMarshaller();
    schema = sf.newSchema(new File("xml/catalog.xsd"));
    marshaller.setSchema(schema);
    marshaller.setEventHandler(new ValidationEventHandler() {
      // allow unmarshalling to continue even if there are errors
      public boolean handleEvent(ValidationEvent ve) {
        System.err.println(ve.getLinkedException());
        return true;
      }
    });
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
  }

  public void write(OutputStream out, Catalog catalog) throws JAXBException {
    marshaller.marshal(catalog, out);
  }

}
