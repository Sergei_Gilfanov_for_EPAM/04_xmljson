package xmljson.convert;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import xmljson.protocol.Catalog;

public class Main {

  public static void main(String[] args) {
    boolean argsAreRight = false;
    while (true) {
      if (args.length != 3) {
        break;
      }
      if (!args[0].equals("json2xml") && !args[0].equals("xml2json")) {
        break;
      }
      argsAreRight = true;
      break;
    }

    if (!argsAreRight) {
      System.out.println("usage: convert <xml2json|json2xml> <inputFileName> <outputFileName>");
    }

    try (InputStream input = args[1].equals("-") ? (System.in) : (new FileInputStream(args[1]));
        OutputStream output =
            args[2].equals("-") ? (System.out) : (new FileOutputStream(args[2]));) {

      switch (args[0]) {
        case "xml2json":
          xml2json(input, output);
          break;
        case "json2xml":
          json2xlm(input, output);
      }
    } catch (IOException | JAXBException | SAXException e) {
      e.printStackTrace();
    }
  }

  private static void xml2json(InputStream in, OutputStream out) throws JsonGenerationException,
      JsonMappingException, IOException, JAXBException, SAXException {
    XmlReader reader = new XmlReader();
    Catalog catalog = reader.read(in);
    JsonWriter writer = new JsonWriter();
    writer.write(out, catalog);
  }

  private static void json2xlm(InputStream in, OutputStream out)
      throws JsonParseException, JsonMappingException, IOException, JAXBException, SAXException {
    JsonReader reader = new JsonReader();
    Catalog catalog = reader.read(in);
    XmlWriter writer = new XmlWriter();
    writer.write(out, catalog);
  }
}
