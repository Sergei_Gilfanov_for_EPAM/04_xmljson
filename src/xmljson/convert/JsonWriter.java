package xmljson.convert;

import java.io.IOException;
import java.io.OutputStream;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import xmljson.protocol.Catalog;
import xmljson.protocol.EmptyElement;


public class JsonWriter {
  private ObjectMapper mapper;

  public JsonWriter() {
    mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
    mapper.setSerializationInclusion(Include.NON_NULL);
    SimpleModule emptyElementModule =
        new SimpleModule("EmptyElement", new Version(1, 0, 0, null, null, null));
    emptyElementModule.addSerializer(EmptyElement.class, new EmptyElementSerializer());
    mapper.registerModule(emptyElementModule);


  }

  public void write(OutputStream out, Catalog store)
      throws JsonGenerationException, JsonMappingException, IOException {
    mapper.writeValue(out, store);
  }

}
