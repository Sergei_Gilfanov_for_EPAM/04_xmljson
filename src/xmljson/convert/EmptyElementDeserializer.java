package xmljson.convert;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import xmljson.protocol.EmptyElement;

public class EmptyElementDeserializer extends JsonDeserializer<EmptyElement> {
  @Override
  public EmptyElement deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    boolean present = p.getBooleanValue();
    if (present) {
      return new EmptyElement();
    } else {
      return null;
    }
  }
}
