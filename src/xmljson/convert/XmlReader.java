package xmljson.convert;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import xmljson.protocol.Catalog;

public class XmlReader {
  static SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
  JAXBContext context;
  Unmarshaller unmarshaller;
  Schema schema;

  public XmlReader() throws JAXBException, SAXException {
    context = JAXBContext.newInstance(Catalog.class);
    unmarshaller = context.createUnmarshaller();
    schema = sf.newSchema(new File("xml/catalog.xsd"));
    unmarshaller.setSchema(schema);
    unmarshaller.setEventHandler(new ValidationEventHandler() {
      // allow unmarshalling to continue even if there are errors
      public boolean handleEvent(ValidationEvent ve) {
        System.err.println(ve.getLinkedException());
        return true;
      }
    });
  }

  public Catalog read(InputStream in) throws JAXBException {
    return (Catalog) unmarshaller.unmarshal(in);
  }

}
