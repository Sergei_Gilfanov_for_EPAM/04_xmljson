package xmljson.convert;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.DeserializationFeature;

import xmljson.protocol.Catalog;
import xmljson.protocol.EmptyElement;

public class JsonReader {
  private ObjectMapper mapper;

  public JsonReader() {
    mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    SimpleModule emptyElementModule =
        new SimpleModule("EmptyElement", new Version(1, 0, 0, null, null, null));
    emptyElementModule.addDeserializer(EmptyElement.class, new EmptyElementDeserializer());
    mapper.registerModule(emptyElementModule);
  }

  public Catalog read(InputStream in) throws JsonParseException, JsonMappingException, IOException {
    return mapper.readValue(in, Catalog.class);
  }
}
